from NeuralNetworks import Rnn
from SimilarityMeasures import CosineSimilarity
from FeatureExtraction import FeaturesOfTexts, TfIdf
from ChatBot_API import ChatAPI
from ChatBot_SQLgenerate import SQLgenerator

import UnwantedResponses

import random
import json
import numpy as np
import nltk
import re
import urllib3
import time

def set_greeting(user_response):
    global greeting_responses
    greeting_inputs = ("hello", "hi", "greetings", "sup", "what's up", "hey")
    greeting_responses = ["Hi... :)", "hey... :)", "hi there :)",
                          "hello :)",
                          "I am glad! you are talking to me :)"]
    for word in user_response.split():
        if word.lower() in greeting_inputs:
            return random.choice(greeting_responses)


def set_okay():
    okay = ["absolutely", "agreed", "all right", "aye", "by all means", "certainly", "fine", "good", "good enough",
            "just so", "of course", "okay", "positively", "precisely", "sure thing", "surely", "very well", "yea",
            "yep"]
    return random.choice(okay)


def set_data_file(key, session_id, data):
    if session_id is None:
        filename = key
        with open('./ChatBot_json/' + filename, 'w') as file:
            json.dump(data, file)
            # jData = json.dumps(data)
            # file.write(jData)
    else:
        ChatAPI.postdata(key, session_id, data)

def get_data_file(key, session_id):
    if session_id is None:
        filename = key
        data_file = open('./ChatBot_json/' + filename, 'r')
        if data_file.read() == '':
            returndata = {}
        else:
            with open('./ChatBot_json/' + filename) as f:
                returndata = json.load(f)
    else:
        data = ChatAPI.getdata(key, session_id)
        returndata = data
    return returndata


def set_corpus(filename):
    file = open(filename, 'r', errors='ignore')
    raw = file.read()
    raw = raw.lower()
    return raw


def reconsider(condition, user_response, session_id):
    user_input = user_response
    no_attempt = 0
    user_input_para = get_data_file('Temp_user_input.json', session_id)
    if user_input_para is '' and condition is False:
        user_input_para = {'reconsider': condition, 'user_input': user_input, 'no_attempt': no_attempt}
        set_data_file('Temp_user_input.json', session_id, user_input_para)
    elif condition is True:
        no_attempt = user_input_para['no_attempt']
        no_attempt += 1
        user_input_para = {'reconsider': condition, 'user_input': user_input, 'no_attempt': no_attempt}
        set_data_file('Temp_user_input.json', session_id, user_input_para)
    elif condition is False:
        no_attempt = 0
        user_input_para = {'reconsider': condition, 'user_input': user_input, 'no_attempt': no_attempt}
        set_data_file('Temp_user_input.json', session_id, user_input_para)


def checkand(user_response):
    user_response_array = []

    words_list = nltk.word_tokenize(user_response)
    if 'and' in words_list:
        tags = nltk.pos_tag(words_list)
        grammer = r"""
                    NandN : {<N.*><CC><N.*>}
                """
        result_parser = nltk.RegexpParser(grammer)
        tree = result_parser.parse(tags)

        middle = []
        for subtree in tree.subtrees():
            if type(subtree) is not tuple and subtree.label() == 'NandN':
                for each_tree in subtree:
                    if each_tree[0] != 'and':
                        middle.append(each_tree[0])
        if len(middle) > 0:
            for each_mid in middle:
                question = ""
                for subtree in tree.subtrees():
                    if subtree.label() is 'S':
                        for each_tree in subtree:
                            if type(each_tree) is tuple:
                                question += " " + each_tree[0]
                            if type(each_tree) is not tuple and each_tree.label() == 'NandN':
                                question += " " + each_mid
                user_response_array.append(question)
        else:
            user_response_array = user_response.split('and')
    else:
        user_response_array = [user_response]

    return user_response_array


class ChatBot:
    def __init__(self, session_id):

        self.c_api = ChatAPI
        self.session_id =  session_id

        # set corpus
        answers = set_corpus('./ChatBot_corpuses/Answer_Corpus.txt')
        defined_questions = set_corpus('./ChatBot_corpuses/Question_Corpus.txt')
        defined_bot_questions = set_corpus('./ChatBot_corpuses/RobotQuestion_Corpus.txt')

        self.main_corpus_data = set_corpus('./ChatBot_corpuses/Corpus.txt')

        # set answers tokens
        self.answers = answers

        # set defined questions tokens
        self.questions = defined_questions

        # set defined bot questions tokens
        self.defined_bot_questions = defined_bot_questions

        self.user_response = ''
        self.bot_response = ''

        # Temp_parameters
        self.t = 0  # Time
        self.index_A = []
        self.index_Ya = []
        parameters = get_data_file('Temp_parameters.json', self.session_id)
        if parameters is '':
            parameters = {'t': self.t, 'index_A': self.index_A, 'index_Ya': self.index_Ya}
            set_data_file('Temp_parameters.json', self.session_id, parameters)
        else:
            self.t = parameters['t']
            self.index_A = parameters['index_A']
            self.index_Ya = parameters['index_Ya']

        # Training data
        self.user_response_array = []
        user_response = get_data_file('Training_data.json', self.session_id)
        if user_response is '':
            data = {'Training_data': self.user_response_array}
            set_data_file('Training_data.json', self.session_id, data)
        else:
            self.user_response_array = user_response['Training_data']

        self.feature_list = []
        feature_list = get_data_file('FeatureLists.json', None)
        if feature_list is '':
            feature_object = FeaturesOfTexts.FeaturesOfTexts(self.main_corpus_data)
            self.feature_list = feature_object.get_feature_list()

            data = {'feature_list': self.feature_list}
            set_data_file('FeatureLists.json', None, data)
        else:
            self.feature_list = feature_list['feature_list']

        # Get tf-idf values of Questions
        self.question_tfidf_data = []
        tfidf_data = get_data_file('TfIdfData.json', None)
        if tfidf_data is '':
            q = TfIdf.TfIdf()
            q.set_raw_feature_data(self.questions, self.feature_list)
            question_tfidf_data = q.get_sentences_tfidf()
            self.question_tfidf_data = question_tfidf_data
            data = {'question_tfidf_data': self.question_tfidf_data}
            set_data_file('TfIdfData.json', None, data)
        else:
            tfidf_data_para = get_data_file('TfidfData_para.json', None)
            if tfidf_data_para["ismodified"] is True:
                # need to rewrite TfIdfData.json FeatureList.json
                # Set feature list
                feature_object = FeaturesOfTexts.FeaturesOfTexts(set_corpus('./ChatBot_corpuses/Answer_Corpus.txt'))
                self.feature_list = feature_object.get_feature_list()

                data = {'feature_list': self.feature_list}
                set_data_file('FeatureLists.json', None, data)

                # Get tf-idf values of Questions
                questions = set_corpus('./ChatBot_corpuses/Question_Corpus.txt')
                q = TfIdf.TfIdf()
                q.set_raw_feature_data(questions, self.feature_list)
                question_tfidf_data = q.get_sentences_tfidf()
                self.question_tfidf_data = question_tfidf_data
                data = {'question_tfidf_data': self.question_tfidf_data}
                set_data_file('TfIdfData.json', None, data)

                tfidf_data_para = {"ismodified": False}
                set_data_file('TfidfData_para.json', None, tfidf_data_para)
            else:
                self.question_tfidf_data = tfidf_data['question_tfidf_data']

        self.question_similarity = CosineSimilarity.CosineSimilarity()

        self.idy = -1

        to_ask_parameters = get_data_file('to_ask.json', self.session_id)
        if to_ask_parameters is '':
            to_ask = False
            to_ask_parameters = {'to_ask': to_ask}
            set_data_file('to_ask.json', self.session_id, to_ask_parameters)
        else:
            self.to_ask = to_ask_parameters['to_ask']

    def get_bot_question(self):
        # user_responses = '. \n'.join(self.user_response_array)
        user_responses = self.user_response_array

        feature_list = self.feature_list
        defined_question_tfidf_data = self.question_tfidf_data

        t2 = TfIdf.TfIdf()
        t2.set_raw_feature_data(user_responses, feature_list)
        user_inputs_tfidf_data = t2.get_sentences_tfidf()

        training_data = np.array(user_inputs_tfidf_data)[:-1]
        testing_data = np.array(user_inputs_tfidf_data)[1:]

        # use RNN from Neural Network package
        feature_size = np.array(user_inputs_tfidf_data).shape[1]

        # create a model in RNN
        model = Rnn.RNN(feature_size)

        # train the model to update parameters using back-propagation and calculate loss
        model.sgd_training_model(model, training_data, testing_data)

        prediction_array = model.predict(training_data)
        bot_q_similarity = CosineSimilarity.CosineSimilarity()

        # get bot question index
        prediction = prediction_array
        sentences_data = np.array(defined_question_tfidf_data)

        cosine_array = bot_q_similarity.get_cosine_array_for_bot(prediction, sentences_data)

        bot_question_array = re.split(r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.)\s', self.defined_bot_questions)

        parameters = get_data_file('Temp_parameters.json', self.session_id)
        index_A = parameters['index_A']
        self.index_Ya = parameters['index_Ya']
        self.idy = -1
        max_value = 0
        bot_response = ''
        for i in range(len(cosine_array)):
            bot_question = bot_question_array[i]
            if (i not in index_A) and (i not in self.index_Ya) and (bot_question != '.'):
                if max_value < cosine_array[i]:
                    max_value = cosine_array[i]
                    self.idy = i
                    bot_response = bot_question_array[self.idy]
                    reconsider(False, "", self.session_id)

        self.index_Ya.append(self.idy)
        parameters = {'t': self.t, 'index_A': index_A, 'index_Ya': self.index_Ya}
        set_data_file('Temp_parameters.json', self.session_id, parameters)
        print(bot_response)

    def set_response(self, user_response, flag, t, session_id):
        user_response = user_response.lower()
        self.t = t
        ct1 = time.time()
        ct2 = 0
        ct3 = 0
        ct4 = 0
        ct5 = 0
        ct6 = 0
        ct7 = 0
        ct8 = 0
        ct9 = 0
        ct10 = 0
        ct11 = 0
        if flag is True:
            if user_response == 'bye':
                flag = False
                self.bot_response = 'Bye! take care..'
                reconsider(False, user_response, session_id)
                ct2 = time.time()
                # print("<br>To say bye:", ct2-ct1)
            elif user_response == 'thanks' or user_response == 'thank you':
                flag = False
                to_ask_parameters = {'to_ask': self.to_ask}
                set_data_file('to_ask.json', self.session_id, to_ask_parameters)
                self.bot_response = 'you are welcome..'
                reconsider(False, user_response, session_id)
                # need to add script to check conversation end words
                ct3 = time.time()
                # print("<br>To say welcome: ",ct3 - ct2," Total: ",ct3 - ct1)
            elif set_greeting(user_response) is not None:
                self.bot_response = set_greeting(user_response)
                reconsider(False, user_response, session_id)
                ct4 = time.time()
                # print("<br>To say greetings: ", ct4 - ct3, " Total: ", ct4 - ct1)
            else:
                ct5 = time.time()
                # print("<br>To get start to give answer: ", ct5 - ct1)
                if user_response[-1] is not ".":
                    user_response = user_response + "."

                # get cosine similarity values
                feature_list = self.feature_list
                question_tfidf_data = self.question_tfidf_data

                u = TfIdf.TfIdf()
                u.set_raw_feature_data(self.questions, feature_list)
                user_tfidf_data = u.get_user_response_tfidf(user_response)
                ct52 = time.time()
                # print("<br>To get user tfidf values: ", ct52 - ct5, " Total: ", ct52 - ct1)
                cosine_values = self.question_similarity.get_cosine_array(user_tfidf_data, question_tfidf_data)
                ct6 = time.time()
                # print("<br>To get cosine value: ", ct6 - ct52, " Total: ", ct6 - ct1)
                # print(cosine_values)
                max_cosine_value = np.max(cosine_values[:-1])

                # print(max_cosine_value)
                answer_index = 0
                # for i in range(len(cosine_values) - 1):
                #     if max_cosine_value == cosine_values[i]:
                #         self.index_A.append(i)
                #         answer_index = i
                #         if max_cosine_value > 0.90:
                #             self.t += 1
                #         parameter_data = {'t': self.t, 'index_A': self.index_A, 'index_Ya': self.index_Ya}
                #         set_data_file('Temp_parameters.json', self.session_id, parameter_data)
                #
                #         # Collect user's responses to input for RNN
                #         self.user_response_array.append(user_response)
                #         data = {'Training_data': self.user_response_array}
                #         set_data_file('Training_data.json', self.session_id, data)

                if max_cosine_value in cosine_values:
                    ct66 = time.time()
                    i = cosine_values.tolist().index(max_cosine_value)
                    ct65 = time.time()
                    # print("<br>To get i value: ", ct65 - ct66, " Total: ", ct65 - ct1)
                    # # s1 = repr(type(i))
                    # # print("<br><br> 1. ","i Value:", i," type: ", s1)
                    # # i2 = np.where(cosine_values == max_cosine_value)[0][0]
                    # # s2 = repr(type(i2))
                    # # print("<br> 2. ", "i Value:", i2, " type: ", s2,"<br><br>")
                    # i = int(np.where(cosine_values == max_cosine_value)[0][0])

                    self.index_A.append(i)
                    answer_index = i
                    if max_cosine_value > 0.90:
                        self.t += 1
                    ct64 = time.time()
                    # print("<br>To increase t: ", ct64 - ct65, " Total: ", ct64 - ct1)


                    parameter_data = {'t': self.t, 'index_A': self.index_A, 'index_Ya': self.index_Ya}
                    set_data_file('Temp_parameters.json', self.session_id, parameter_data)

                    ct63 = time.time()
                    # print("<br>To save parameter values: ", ct63 - ct64, " Total: ", ct63 - ct1)
                    # Collect user's responses to input for RNN
                    self.user_response_array.append(user_response)
                    data = {'Training_data': self.user_response_array}
                    set_data_file('Training_data.json', self.session_id, data)
                    ct62 = time.time()
                    # print("<br>To get max cosine value index: ", ct62 - ct63, " Total: ", ct62 - ct1)

                if max_cosine_value > 0.90:  # Level 1
                    answers = self.answers.split(".")
                    self.bot_response = answers[answer_index]
                    #self.bot_response = nltk.sent_tokenize(self.answers)[answer_index]
                    reconsider(False, user_response, session_id)
                    ct7 = time.time()
                    # print("<br>To check reconsider: ", ct7 - ct62, " Total: ", ct7 - ct1)
                # get SQL data
                if max_cosine_value < 0.90:  # and max_cosine_value > 0.85:   #Level 2

                    un_obj = UnwantedResponses.UnwantedCorpus(user_response,session_id)
                    if un_obj.checkcorpus() != None:
                        self.bot_response = un_obj.checkcorpus()
                        ct8 = time.time()
                        #print("<br>To get answer from unwanted part: ", ct8 - ct7, " Total: ", ct8 - ct1)
                    else:
                        statment = SQLgenerator.sqlStatment(user_response, session_id)
                        sql_result = statment.setSQLParameters()
                        if len(sql_result) != 0:
                            self.bot_response = repr(sql_result)+"check"
                            ct9 = time.time()
                            # print("<br>To get answer from sql: ", ct9 - ct8, " Total: ", ct9 - ct1)
                        elif len(sql_result) == 0:
                            # elif sql_para['type'] is None:  # Level 3

                            user_input_para = get_data_file('Temp_user_input.json', self.session_id)
                            no_attempt = user_input_para['no_attempt']
                            if no_attempt < 1:
                                self.bot_response = "Sorry I can't understand you. Can you tell me more details about your question. Such as purpose, reason, etc. "
                                reconsider(True, user_response, self.session_id)
                                ct10 = time.time()
                                # print("<br>To tell re renter details: ", ct10 - ct9, " Total: ", ct10 - ct1)
                            else:  # Level 4
                                self.bot_response = 'Sorry I couldn\'t help you for your question about ' + repr(
                                    user_response) + '. You may contact us through sending email to our support team'
                                ct11 = time.time()
                                # print("<br>To tell contact team: ", ct11 - ct10, " Total: ", ct11 - ct1)
                                # report_data = get_data_file('./report/report.json')
                                data_file = open('./Report/report.json', 'r')
                                if data_file.read() == '':
                                    report_data = ''
                                else:
                                    with open('./Report/report.json') as f:
                                        report_data = json.load(f)

                                user_inputs = []
                                if report_data is '':
                                    report = {'user_inputs': user_inputs}
                                    # set_data_file('./report/report.json', report)
                                    with open('./Report/report.json', 'w') as file:
                                        json.dump(report, file)
                                else:
                                    user_inputs = report_data['user_inputs']
                                    # user_inputs.append(user_input_para['user_input'])
                                    user_inputs.append(user_response)
                                    report = {'user_inputs': user_inputs}
                                    # set_data_file('./report/report.json', report)
                                    with open('./Report/report.json', 'w') as file:
                                        json.dump(report, file)

                                    # remove that user input from Temp_user_input.json
                                    user_input_data = {"reconsider": False, "user_input": "", "no_attempt": 0}
                                    set_data_file('Temp_user_input.json', self.session_id, user_input_data)


            #print(ct1,ct2,ct3,ct4,ct5,ct6,ct7,ct8,ct9,ct10,ct11)
            #print("\n")
            #print("[Total time: ", time.time()- ct1,"]   ")

            return self.bot_response, flag
