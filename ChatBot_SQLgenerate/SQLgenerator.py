import nltk
import json

from ChatBot_API import ChatAPI

class sqlStatment:
    def __init__(self, user_response, session_id):
        self.user_response = user_response
        self.user_id = session_id
        self.attributes = []
        self.values= []
        self.condition = []

    def getTags(self, user_response):
        words = nltk.word_tokenize(user_response)
        tags = nltk.pos_tag(words)
        return tags
    """
    def setSQLParameters(self):
        tags = sqlStatment.getTags(self, self.user_response)
        getQueryData = r'''
                Attribute : {<NN.*>}
                AttributeTarget: {<Attribute><CC><Attribute>+}
                Prefix: {<DT|JJ>}
                AttributeSection: {<Prefix><AttributeTarget><Postfix>}
                Condition: {<V.*>|<JJR><IN>|<JJS><IN>|<JJR>|<JJS>|<RBR>|<RBR><IN>|<NNS>}
                values: {<CD>}
                '''
        resultParser = nltk.RegexpParser(getQueryData)
        result = resultParser.parse(tags)

        for subtree in result.subtrees():
            if subtree.label() == "AttributeTarget":
                for each_subtree in subtree:
                    if type(each_subtree) is not tuple and each_subtree.label() == "Attribute":
                        #self.attributes.append(each_subtree[0][0]) #removed temporary
                        self.values.append(each_subtree[0][0])
            elif subtree.label() == "Attribute" and subtree[0][0] not in self.attributes:
                self.values.append(subtree[0][0])
            elif subtree.label() == "values" and subtree[0][0] not in self.attributes:
                self.values.append(subtree[0][0])

        for subtree in result.subtrees():
            if subtree.label() == "Condition":
                for each_data in subtree:
                    #self.condition = each_data[0]
                    self.condition.append(each_data[0])

        sqlParameters = {"query": self.user_response,"attributes": self.attributes, "values":self.values, "condition": self.condition}
        return sqlParameters
    """

    def setSQLParameters(self):

        # defined conditions, product category, product sub category
        c_list = ['max', 'min', 'minimum', 'maximum', 'less', 'more', 'is','equal', 'are', 'between']

        # # conditions list
        # # 'are' or chack quality of few values
        # pc_list = ['foods', 'vegetables']  #product_category list
        # psc_list = ['apple', 'orange', 'carrot', 'potato' ]  #product_subcategory list
        # product_type = ['PC','PSC']

        nouns = []
        values = []
        tags = sqlStatment.getTags(self, self.user_response)
        getQueryData = r"""
                        nouns: {<NN.*>+}
                        than:{<JJR><IN><CD>|<RBR><IN><CD>}
                        between: {<IN><CD><CC><CD>}
                        values: {<CD>}
                        """
        #print(tags)
        resultParser = nltk.RegexpParser(getQueryData)
        result = resultParser.parse(tags)
        for subtree in result.subtrees():
            if subtree.label() == "nouns":
                subtree = ' '.join([a[0] for a in subtree])
                nouns.append(subtree)
        #     elif subtree.label() == "values":
        #         subtree = ' '.join([a[0] for a in subtree])
        #         values.append(subtree)
        #     elif subtree.label() == "than":   # less than or more than
        #         subtree = ' '.join([a[0] for a in subtree])
        #         values.append(subtree)
        #     elif subtree.label() == "between":
        #         subtree = ' '.join([a[0] for a in subtree])
        #         values.append(subtree)


        words = nltk.word_tokenize(self.user_response)
        conditions = []
        p = nltk.PorterStemmer()
        for word in words:
            if word is 'equals':
                word = p.stem(word)
            if 'not' is words[words.index(word)-1] and word in c_list:
                conditions.append('not ' +word)
            elif word in c_list:
                conditions.append(word)

        # create json data to post

        # all = ['products','items','all','brands','goods','commodities','fruits','productions','stock','manufactures','resources']
        # type = None
        # for noun in nouns:
        #     if noun in pc_list:
        #         type = 'PC' # product category
        #         break
        #     elif noun in all:
        #         type = 'PC'  # product category
        #         nouns.append('all')
        #         break
        #     elif noun in psc_list:
        #         type = 'PSC'  #product sub category


        #return sqlParameters

        #SQldatafile = open('./SQLtargets.txt', 'r', errors='ignore')
        SQldatafile = open('./ChatBot_SQLgenerate/SQLtargets.txt','r', errors='ignore')
        SQLdataRaw = SQldatafile.read().lower().splitlines()
        results = []
        for each_SQLraw in SQLdataRaw:
            TD = each_SQLraw.split(" | ")
            attributes = [noun for noun in nouns if noun in TD[1].split(",")]
            if len(attributes)!=0:
                tablelable = TD[0]
                check_nouns = [word for word in nouns if word not in TD[1].split(",")]
                condition = [con for con in conditions if con in TD[2].split(",")]
                sqlParameters = {"type": tablelable, "nouns": attributes, "conditions": condition,
                                 "values": check_nouns}
                c_api = ChatAPI.postSQldata(self.user_id, sqlParameters)
                results.append(json.loads(c_api.data.decode('utf-8')))

        return results

if __name__ == '__main__':
    sentence = "what is the price of orange?"
    c = sqlStatment(sentence,"1")
    print(c.setSQLParameters())



