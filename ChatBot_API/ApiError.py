class ApiError(Exception):
    def __init__(self):
        self.status = Exception

    def __str__(self):
        return 'ApiError : status = {}'.format(self.status)
