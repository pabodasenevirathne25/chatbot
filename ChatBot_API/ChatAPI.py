# ChatBot API for GET & POST data with the database
import urllib3
import json

#link = 'http://localhost:3000/'
# link = open('./localhost.txt','r').read().lower()
link = 'http://localhost:8088'

def getdata(key,session_id):
    http = urllib3.PoolManager()
    r = http.request('GET',  link+'/json/fetch/'+key+'/'+session_id)
    #r = http.request('GET', link + key + '/' + session_id)
    return json.loads(r.data.decode('utf-8'))

def postdata(key,session_id,data):
    http = urllib3.PoolManager()
    encoded_data = json.dumps(data).encode('utf-8')
    return http.request('GET',  link+'/json/put/'+key+'/'+session_id, body=encoded_data,headers={'Content-Type': 'application/json'})
    #return http.request('PUT', link +key+'/'+session_id, body=encoded_data,headers={'Content-Type': 'application/json'})

def postSQldata(session_id, data):
    http = urllib3.PoolManager()
    encoded_data = json.dumps(data).encode('utf-8')
    return http.request('GET', link+'/json/put/'+'sql_para'+'/'+session_id, body=encoded_data,headers={'Content-Type': 'application/json'})
    #return http.request('PUT', link+'sql_para/' + session_id, body=encoded_data, headers={'Content-Type': 'application/json'})

