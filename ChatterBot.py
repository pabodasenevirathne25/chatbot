from chatterbot.trainers import ListTrainer
from chatterbot import ChatBot as cbot

bot = cbot('Test',
           storage_adapter='chatterbot.storage.SQLStorageAdapter',
           logic_adapters=[
               {
                   'import_path': 'chatterbot.logic.BestMatch',
                   'maximum_similarity_threshold': 0.50
               }
           ])

conv = open("ChatBot_corpuses/Corpus.txt", "r").readlines()

trainer = ListTrainer(bot)
A =  trainer.train(conv)

# Q = open("ChatBot_corpuses/Question_corpus.txt", "r").readlines()
# A = open("ChatBot_corpuses/Answer_corpus.txt", "r").readlines()
#
# QA = open("ChatBot_corpuses/Doxpro-domainQA.txt", "a+")
#
# for i in range(len(Q)):
#     QA.write(Q[i])
#     QA.write(A[i])

while True:
    request = input('You: ')

    response = bot.get_response(request)

    print('Bot: ', response)




