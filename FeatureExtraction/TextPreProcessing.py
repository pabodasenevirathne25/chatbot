import unicodedata
import re

class TextPreProcess:
    def __init__(self, tokens):
        self.tokens = tokens

    def set_text_tokens(self, tokens):  # text pre-processing on words
        # Removing accented characters
        filter_1 = []
        for t in tokens:
            t = unicodedata.normalize('NFKD', t).encode('ascii', 'ignore').decode('utf-8', 'ignore')
            filter_1.append(t)

        # Expanding Contractions
        # ______ to be coding ________

        # Removing Special Characters - except digits and characters
        filter_2 = []
        pattern = r'[^a-zA-Z0-9]'
        for t in filter_1:
            t = re.sub(pattern, '', t)
            filter_2.append(t)

        # Stemming
        """
        array_3 = []
        ps = nltk.porter.PorterStemmer()
        for t in filter_2:
            t = ps.stem(t)
            array_3.append(t)
        filter_2 = array_3
        """
        # ______or can be use Lemmatization ________

        # Remove stop words
        # ______ no need now ______
        """stop_words = ["i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours", 
        "yourself", "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its", 
        "itself", "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that", 
        "these", "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had", "having", 
        "do", "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or", "because", "as", "until", "while", 
        "of", "at", "by", "for", "with", "about", "against", "between", "into", "through", "during", "before", 
        "after", "above", "below", "to", "from", "up", "down", "in", "out", "on", "off", "over", "under", "again", 
        "further", "then", "once", "here", "there", "when", "where", "why", "how", "all", "any", "both", "each", 
        "few", "more", "most", "other", "some", "such", "no", "nor", "not", "only", "own", "same", "so", "than", 
        "too", "very", "s", "t", "can", "will", "just", "don", "should", "now"] array_4 = [] for t in filter_2: if t 
        not in stop_words: array_4.append(t) filter_2 = array_4 """
        # Which is better to use stemming or remove stop words for chatBot?

        self.tokens = filter_2

    def get__text_tokens(self):
        self.set_text_tokens(self.tokens)
        return self.tokens

if __name__ == '__main__':
    test = TextPreProcess("what is the price of mango")

