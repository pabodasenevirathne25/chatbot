import nltk
import math
import json
import time
import multiprocessing

from FeatureExtraction import TextPreProcessing
from multiprocessing import Process, Pool , freeze_support, cpu_count

def set_data_file(filename, data):
    # save parameters in JSON file
    with open('../ChatBot_json/'+filename, 'w') as file:
        json.dump(data, file)

def set_corpus(filename):
    file = open(filename, 'r', errors='ignore')
    raw = file.read()
    raw = raw.lower()
    return raw

def get_data_file(filename):
    data_file = open('../ChatBot_json/'+filename, 'r')
    if data_file.read() == '':
        data = ''
    else:
        with open('../ChatBot_json/'+filename) as f:
            data = json.load(f)
    return data


def text_pre_process(sentence):
    sentence_tokens = nltk.word_tokenize(sentence)
    try:
        words_list = TextPreProcessing.TextPreProcess(sentence_tokens).get__text_tokens()
    except IndexError:
        words_list = []
        print('Error: Empty word_list in TfIdf.py')
    return words_list


def get_tf_value(word, sentence):
    words_list = text_pre_process(sentence)
    count = words_list.count(word)
    if len(words_list) is 0:
        return 0
    else:
        return count / len(words_list)

def get_idf_value(word, sentences):
    count = 0
    for sentence in sentences:
        if word in sentence.split():
            count += 1
    idf_value = 1 + math.log(len(sentences) / 1 + count)
    return idf_value

# def get_idf_value(word, sentences):
#     count = 0
#     for sentence in sentences:
#         words_list = text_pre_process(sentence)
#         if word in words_list:
#             count += 1
#     idf_value = 1 + math.log(len(sentences) / 1 + count)
#     return idf_value


class TfIdf:
    def __init__(self):
        self.question_2d_tiarray = []
        self.user_response_tiarray = []
        self.sentences = ''
        self.features_list = []

    def set_raw_feature_data(self, raw_data, features_list):  # raw data --> multiple sentences
        if type(raw_data) is not list:
            self.sentences = raw_data.split(".")#nltk.sent_tokenize(raw_data)
        else:
            self.sentences = raw_data
        self.features_list = features_list

    # def read_tfidf(self, SSW):
    #     sentence = SSW[0]
    #     word_feature = SSW[1]
    #     #st = time.time()
    #     tf = get_tf_value(word_feature, sentence)
    #     #tf_time = time.time()
    #     #print("calculate TF:", tf_time - st)
    #     idf = get_idf_value(word_feature, self.sentences)
    #     #print("calculate idf:",time.time() - tf_time)
    #     tf_idf_value = tf * idf
    #     return tf_idf_value

    # def get_tf_idf_sentence_array(self, sentence):
    #     sentence_tf_idf = []
    #     pool = Pool(processes=cpu_count())
    #     sentence_tf_idf = pool.map(self.read_tfidf, [(sentence, word) for word in self.features_list])
    #     pool.close()
    #     pool.join()
    #     return sentence_tf_idf

    def get_tf_idf_sentence_array(self, sentence):
        sentence_tf_idf = []
        for word_feature in self.features_list:
            tf = get_tf_value(word_feature, sentence)
            idf = get_idf_value(word_feature, self.sentences)
            tf_idf_value = tf * idf
            sentence_tf_idf.append(tf_idf_value)
        return sentence_tf_idf

    def get_user_response_tfidf(self, user_response):
        self.user_response_tiarray = self.get_tf_idf_sentence_array(user_response)
        return self.user_response_tiarray

    def read_sentence(self, sentence):
        return self.get_tf_idf_sentence_array(sentence)

    def get_sentences_tfidf(self):
        self.question_2d_tiarray = []
        for sentence in self.sentences:
            self.question_2d_tiarray.append(self.get_tf_idf_sentence_array(sentence))
        return self.question_2d_tiarray
        # sentences = self.sentences
        # pool = Pool(processes=cpu_count())
        # self.question_2d_tiarray = pool.map(self.read_sentence, sentences)
        # pool.close()
        # pool.join()
        # return self.question_2d_tiarray


if __name__ == '__main__':

    user_response = "what is commodity exchange"
    start = time.time()
    defined_questions = set_corpus('../ChatBot_corpuses/Question_Corpus.txt')
    t1 = time.time()
    print("to read questions :", t1 - start," seconds")

    questions = defined_questions
    feature_list = get_data_file('FeatureLists.json')
    feature_list = feature_list['feature_list']
    t2 = time.time()
    print("to get feature list :", t2 - t1, " seconds")

    c = TfIdf()
    t3 = time.time()
    print("to create tfidf object :", t3 - t2, " seconds")

    c.set_raw_feature_data(questions, feature_list)
    t4 = time.time()
    print("to set raw features:", t4 - t3, " seconds")

    user_tfidf_data = c.get_user_response_tfidf(user_response)
    t5 = time.time()
    print("to get tfidf data :", t5 - t4, " seconds")
    print(user_tfidf_data)

    # Normal : 17.158225536346436  seconds
    # Map: 6.797362565994263  seconds