import cgitb
import json
import sys

cgitb.enable() #for debugging

def get_data_file(filename):
    data_file = open(filename, 'r')
    if data_file.read() == '':
        data = ''
    else:
        with open(filename) as f:
            data = json.load(f)
    return data

def set_data_file(filename, data):
    # save parameters in JSON file
    with open(filename, 'w') as file:
        json.dump(data, file)


# Step 1: check in report.json
report_data = get_data_file('report.json')
user_inputs = report_data['user_inputs']

question = sys.argv[1]
answer = sys.argv[2]

if answer is not None and question is not None:
    if question in user_inputs:

        Qf = open("../ChatBot_corpuses/Question_corpus.txt", "a+")
        Qf.write("\n%s" % question)

        Af = open("../ChatBot_corpuses/Answer_corpus.txt", "a+")
        Af.write("\n%s" % answer)

        Bf = open("../ChatBot_corpuses/RobotQuestion_Corpus.txt", "a+")
        BotQuest = "Do you like to know "+repr(question)
        Bf.write("\n%s" % BotQuest)

        # remove first question from the list
        user_inputs.pop(user_inputs.index(question))

        report = {'user_inputs': user_inputs}
        set_data_file('report.json', report)
    else:
        print('question is not found in reported user responses.')






