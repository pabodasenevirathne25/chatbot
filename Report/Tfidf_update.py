import cgitb
import json
import sys

from FeatureExtraction import FeaturesOfTexts, TfIdf

cgitb.enable()  # for debugging


def get_data_file(filename):
    data_file = open(filename, 'r')
    if data_file.read() == '':
        jsondata = ''
    else:
        with open(filename) as f:
            jsondata = json.load(f)
    return jsondata


def set_data_file(filename, jsondata):
    with open("../ChatBot_json/" + filename, 'w') as file:
        json.dump(jsondata, file)


def set_corpus(filename):
    file = open(filename, 'r', errors='ignore')
    raw = file.read()
    raw = raw.lower()
    return raw


is_Update = sys.argv[1]
print(is_Update)

if is_Update == "True":
    # Set feature list
    feature_object = FeaturesOfTexts.FeaturesOfTexts(set_corpus('../ChatBot_corpuses/Answer_Corpus.txt'))
    feature_list = feature_object.get_feature_list()
    data = {'feature_list': feature_list}
    set_data_file('FeatureLists.json', data)
    # Get tf-idf values of Questions
    questions = set_corpus('../ChatBot_corpuses/Question_Corpus.txt')
    q = TfIdf.TfIdf()
    q.set_raw_feature_data(questions, feature_list)
    question_tfidf_data = q.get_sentences_tfidf()
    question_tfidf_data = question_tfidf_data
    data = {'question_tfidf_data': question_tfidf_data}
    set_data_file('TfIdfData.json', data)
