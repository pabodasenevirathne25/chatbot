import nltk


from FeatureExtraction import TextPreProcessing


def set_corpus(filename):
    file = open(filename, 'r', errors='ignore')
    raw = file.read()
    raw = raw.lower()
    return raw


class CorpusGenerator:
    def __init__(self, raw_data):
        self.raw_data = raw_data
        self.sentences = nltk.sent_tokenize(self.raw_data)

    def get_sentences(self):
        return self.sentences

    @staticmethod
    def get_words(sent):
        words = nltk.word_tokenize(sent)
        t = TextPreProcessing.TextPreProcess(words)
        words = t.get__text_tokens()
        return words

    @staticmethod
    def get_word_tags(w):
        # tag = [key for key, array in self.dictionary.items() if w in array]
        tag = nltk.pos_tag(w)
        return tag


if __name__ == '__main__':
    raw = set_corpus('Corpus.txt')
    cg = CorpusGenerator(raw)
    sentences = cg.get_sentences()

    # sentences = ['To create a Workflow, first you need to login', 'Barcode Analyser is a standalone component that ']
    # sentences = {'As recently as 12500 years ago, the Earth was in the midst of a glacial age referred to as the Last'
    #             'Ice Age'}
    # sentences = {"+"}
    # sentences = {"Hinton is a British cognitive psychologist and computer scientist most noted for his work on artificial neural networks."}
    # sentences = {"am, is, are, was, were, been, be, did, worked, works, working"}
    # sentences = {"a car","an emplyee", "cars","123","there exists","there is", "there"}
    # sentences = {"VALLIBEL FINANCE PLC is a private sector."}
    # sentences = {"scanning and indexing is about once the documents are scanned"}
    index = -1
    no_sentence = 0

    # file_A = open("Answer_corpus.txt", "r")
    # contents_A = file_A.readlines()
    # file_A.close()
    #
    # file_Q = open("Question_corpus.txt", "r")
    # contents_Q = file_Q.readlines()
    # file_Q.close()
    #
    # file_B = open("RobotQuestion_Corpus.txt", "r")
    # contents_B = file_B.readlines()
    # file_B.close()
    #
    # file_A_write = open("Answer_corpus.txt", "w")
    # file_Q_write = open("Question_corpus.txt", "w")
    # file_B_write = open("RobotQuestion_Corpus.txt", "w")

    Qf = open("Question_corpus.txt", "a+")
    Af = open("Answer_corpus.txt", "a+")
    Bf = open("RobotQuestion_Corpus.txt", "a+")

    answers = []
    questions = []
    Botquestions = []
    Addquestion1 = []
    Addquestion2 = []
    Addquestion3 = []

    for sentence in sentences:
        #print(sentence)
        word = nltk.word_tokenize(sentence)
        tags = cg.get_word_tags(word)

        grammar1 = r"""
        NP: {<DT|PDT|JJ|CC|NN.*|CC|IN|VBG>+}
        V: {<VB|VBD|VBN|VBP|VBZ>}
        WHAT_1: {<NP><V>}
        """
        cp = nltk.RegexpParser(grammar1)
        result = cp.parse(tags)
        toprint = ""
        sent1 = ""
        for subtree in result.subtrees():
            if subtree.label() == "WHAT_1":
                what_1 = {1: "What ", 2: "", 3: "", 4: "?"}  # Question structure
                for sub_subtree in subtree:
                    if sub_subtree.label() == "V":
                        for element in sub_subtree:
                            what_1[2] += element[0] + " "
                    elif sub_subtree.label() == "NP":
                        for element in sub_subtree:
                            what_1[3] += element[0] + " "

                if what_1[2] != "" and what_1[3] != "":
                    sent1 = ' '.join([tag[0] for tag in list(tags)])
                    for value in what_1.values():
                        toprint += value
                    quests = toprint.split("?")
                    for quest in quests:
                        if quest is not '':
                            if quest not in Addquestion1:
                                Qf.write("\n%s" % quest+".")
                                Af.write("\n%s" % sent1)
                                BotQuest = "Do you like to know " + quest + "? (Please enter 'YES' or 'NO')."
                                Bf.write("\n%s" % BotQuest)
                                Addquestion1.append(quest)



        grammar2 = r"""
                       NP: {<DT|PDT|JJ|CC|NN.*|CC|IN>+}
                       V: {<V.*>}
                       WHAT_2: {<V><NP>}
                       """
        cp2 = nltk.RegexpParser(grammar2)
        result2 = cp2.parse(tags)
        toprint = ""
        sent2 = ""
        for subtree in result2.subtrees():

            if subtree.label() == "WHAT_2":
                what_2 = {1: "What ", 2: "", 3: "", 4: "?"}
                for sub_subtree in subtree:
                    if sub_subtree.label() == "V":
                        for element in sub_subtree:
                            what_2[2] += element[0] + " "
                    elif sub_subtree.label() == "NP":
                        for element in sub_subtree:
                            what_2[3] += element[0] + " "

                if what_2[2] != "" and what_2[3] != "":
                    sent2 = ' '.join([tag[0] for tag in list(tags)])
                    for value in what_2.values():
                        toprint += value
                    quests = toprint.split("?")
                    for quest in quests:
                        if quest is not '':
                            if quest not in Addquestion2:
                                Qf.write("\n%s" % quest+".")
                                Af.write("\n%s" % sent2)
                                BotQuest = "Do you like to know " + quest + "? (Please enter 'YES' or 'NO')."
                                Bf.write("\n%s" % BotQuest)
                                Addquestion2.append(quest)



        grammar3 = r"""
                               NP: {<DT|PDT|JJ|CC|NN.*|CC|IN>+}
                               V: {<V.*>}
                               T: {<TO>}
                               MANNER: {<T|V>}
                               HOW: {<MANNER><NP>+}
                               """
        cp3 = nltk.RegexpParser(grammar3)
        result3 = cp3.parse(tags)
        toprint = ""
        sent3 = ""
        for subtree in result3.subtrees():
            if subtree.label() == "HOW":
                how_1 = {1: "How ", 2: "", 3: "", 4: "", 5: "?"}
                for sub_subtree in subtree:
                    if sub_subtree.label() == "MANNER":
                        for sub_sub_subtree in sub_subtree:
                            if sub_sub_subtree.label() == "T":
                                for element in sub_sub_subtree:
                                    how_1[2] += element[0] + " "
                            elif sub_sub_subtree.label() == "V":
                                for element in sub_sub_subtree:
                                    how_1[3] += element[0] + " "
                    if sub_subtree.label() == "NP":
                        for element in sub_subtree:
                            how_1[4] += element[0] + " "

                if how_1[2] != "":
                    sent3 = ' '.join([tag[0] for tag in list(tags)])
                    for value in how_1.values():
                        toprint += value
                    quests = toprint.split("?")
                    for quest in quests:
                        if quest is not '':
                            if quest not in Addquestion3:
                                Qf.write("\n%s" % quest+".")
                                Af.write("\n%s" % sent3)
                                BotQuest = "Do you like to know " + quest + "? (Please enter 'YES' or 'NO')."
                                Bf.write("\n%s" % BotQuest)
                                Addquestion3.append(quest)