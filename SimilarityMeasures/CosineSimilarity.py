import math
import numpy as np
import time

from multiprocessing import Process, Pool , freeze_support, cpu_count

def get_cosine_value(v1, v2):

    dot_sum = 0.0
    sum_norm1 = 0.0
    sum_norm2 = 0.0
    v1 = np.array(v1)
    v2 = np.array(v2)
    for i in range(len(v1)):
        x = v1[i]
        y = v2[i]
        # Dot product
        dot_sum += x * y
        # Norm
        sum_norm1 += x * x
        sum_norm2 += y * y

    if math.sqrt(sum_norm1 * sum_norm2) > 0:
        return dot_sum / math.sqrt(sum_norm1 * sum_norm2)
    else:
        return 0


class CosineSimilarity:
    """
    @staticmethod
    def get_cosine_array(sentence, raw_data):
        tf_idf = TfIdf.TfIdf(raw_data)
        cosine_array = []
        user_input_array = tf_idf.get_tf_idf_sentence_array(sentence)
        sentences_array = tf_idf.get_tf_idf_2d_array()

        for each_sentence_vector in sentences_array:
            cosine_array.append(get_cosine_value(user_input_array, each_sentence_vector))
        # return as a numpy array
        return np.array(cosine_array)
    """

    # def read_values(self, array):
    #     array1 = array[0]
    #     array2 = array[1]
    #     return get_cosine_value(array1, array2)

    @staticmethod
    def get_cosine_array(user_response_vector, question_vectors):
        cosine_array = []
        for each_vector in question_vectors:
            cosine_array.append(get_cosine_value(user_response_vector, each_vector))
        # return as a numpy array
        return np.array(cosine_array)
        # u_vector = user_response_vector
        # q_vectors = question_vectors
        # pool = Pool(processes=cpu_count())
        # cosine_array = pool.map(CosineSimilarity().read_values, [(u_vector, q_v) for q_v in q_vectors])
        # pool.close()
        # pool.join()
        # return np.array(cosine_array)

    # @staticmethod
    # def get_cosine_array(user_response_vector, question_vectors):
    #     cosine_array = []
    #     for each_vector in question_vectors:
    #         cosine_array.append(get_cosine_value(user_response_vector, each_vector))
    #     # return as a numpy array
    #     return np.array(cosine_array)

    @staticmethod
    def get_cosine_array_for_bot(predicted_array, d2_array):
        cosine_array = []
        for each_vector in d2_array:
            cosine_array.append(get_cosine_value(predicted_array, each_vector))
        return np.array(cosine_array)

if __name__ == '__main__':
    start = time.time()
    s = CosineSimilarity()
    A = [[1,2,3,4,5,6], [2,3,4,5,6,7]]
    B = [2,3,4,5,6,7]
    print(s.get_cosine_array(B,A))
    end = time.time()
    print("total time:", end - start)