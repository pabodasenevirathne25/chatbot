import nltk
import json
import numpy as np
#import enchant  #  need to install enchant in python. License: Enchant is licensed under the LGPL license with an Exception so that non-free plugin backends could be loaded and used by Enchant.
                # C:\Users\admin>python -m pip install pyenchant
                # 1. download from https://github.com/AbiWord/enchant
                # 2. follow the instruction https://faculty.math.illinois.edu/~gfrancis/illimath/windows/aszgard_mini/movpy-2.0.0-py2.4.4/manuals/PyEnchant/PyEnchant%20Tutorial.htm

from nltk.corpus import words as w

from FeatureExtraction import FeaturesOfTexts, TfIdf
from SimilarityMeasures import CosineSimilarity


def get_data_file(filename):
    data_file = open(filename, 'r')
    if data_file.read() == '':
        data = ''
    else:
        with open(filename) as f:
            data = json.load(f)
    return data


def set_data_file(filename, data):
    with open(filename, 'w') as file:
        json.dump(data, file)


class UnwantedCorpus:
    def __init__(self, user_response, session_id):
        if user_response[-1]!=".":
            self.user_response = user_response+"."
        else: self.user_response = user_response
        user_wordlist = nltk.word_tokenize(user_response)

        self.user_id = session_id

        self.Un_QCorpus = open('./ChatBot_corpuses/Unwanted_QCorpus.txt', 'r', errors='ignore').read().lower()

        #  Calculate unwanted feature array
        U_feature_data = get_data_file('./ChatBot_json/U_features.json')

        need_TfidfUpdate = False

        if U_feature_data is '':
            feature_object = FeaturesOfTexts.FeaturesOfTexts(self.Un_QCorpus)
            self.feature_list = feature_object.get_feature_list()

            self.U_feature_data = self.feature_list
            set_data_file('./ChatBot_json/U_features.json', {"feature_list": self.U_feature_data})
        elif [word for word in user_wordlist if word not in U_feature_data['feature_list']]:  #  to check user response has new words than exsiting feature array
            feature_object = FeaturesOfTexts.FeaturesOfTexts(self.Un_QCorpus + "\n" + self.user_response)
            self.feature_list = feature_object.get_feature_list()
            self.U_feature_data = self.feature_list
            need_TfidfUpdate = True
        else:
            self.U_feature_data = U_feature_data['feature_list']

        U_TfIdf = TfIdf.TfIdf()
        U_TfIdf.set_raw_feature_data(self.Un_QCorpus, self.U_feature_data)

        # calculate user_response TfIdf array
        self.U_TfIdf_userarray = U_TfIdf.get_user_response_tfidf(self.user_response)

        #  Calculate unwanted feature array
        U_TfIdf_data = get_data_file('./ChatBot_json/U_TfIdf.json')

        if U_TfIdf_data is '':
            self.U_TfIdf_corpusarray = U_TfIdf.get_sentences_tfidf()
            self.U_Tfidf_data = {"U_tfidf_data": self.U_TfIdf_corpusarray}
            set_data_file('./ChatBot_json/U_TfIdf.json', self.U_Tfidf_data)
        elif need_TfidfUpdate is True:
            self.U_TfIdf_corpusarray = U_TfIdf.get_sentences_tfidf()
        else:
            self.U_TfIdf_corpusarray = U_TfIdf_data['U_tfidf_data']
        # calculate cosine similarity
        self.U_Cosine_array = CosineSimilarity.CosineSimilarity.get_cosine_array(self.U_TfIdf_userarray,self.U_TfIdf_corpusarray)

    @staticmethod
    def isword(word):
        #d = enchant.Dict("en_US")
        if word in w.words():
            return True
        else:
            return False

    def checkcorpus(self):
        words = nltk.word_tokenize(self.user_response)
        for word in words[:-1]:
            if self.isword(word) is False:
                return "Please insert valid question!"

        # check maximum cosine value
        U_max_c_value = np.max(self.U_Cosine_array)
        if U_max_c_value > 0.90:
            return "Please insert valid question!"
        else:
            return None


if __name__ == '__main__':
    user_input = "What is doxpro and feature of doxpro "
    user_id = "1"
    Un = UnwantedCorpus(user_input, user_id)
    print(Un.checkcorpus())
