# import cgi
# import cgitb
import ChatBot
import json
import nltk
import random
import sys
import time

from ChatBot_API import ChatAPI


# cgitb.enable()  # for debugging


# print("Content-type: text/html")
# print()
# print("<html><head><title>ChatBot</title><meta charset='utf-8'>\
#   <meta name='viewport' content='width=device-width, initial-scale=1''>\
#   <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'>\
#   <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js'></script>\
#   <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js'></script>\
#   <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'></script></head>")
# print("<body>")
#
# print(
#     "Hello. My name is DoxPro-Chatbot. I will answer your problems about DoxPro. If you want to exit, type Bye!<br><br><br><br>")
# print(
#     "<form id='message-form' name='chatbot' action='http://localhost:8080/WebApp_ChatBot/cgi-bin/Repo1"
#     "/ChatBot_web.py' method='get'>")
# print("Me: <input id='messege-input' type='text' size = 100 name='myquery'>")
# #print("Id: <input id='messege-input' type='text' size = 100 name='session_id'>")
# print("<input type='submit' value='send'>")
# print("</form>")


def set_data_file(key, user_session_id, data):
    if user_session_id is None:
        filename = key
        with open('./ChatBot_json/' + filename, 'w') as file:
            json.dump(data, file)
    else:
        ChatAPI.postdata(key, user_session_id, data)


def get_data_file(key, user_session_id):
    if user_session_id is None:
        filename = key
        data_file = open('./ChatBot_json/' + filename, 'r')
        if data_file.read() == '':
            returndata = {}
        else:
            with open('./ChatBot_json/' + filename) as f:
                returndata = json.load(f)
    else:
        data = ChatAPI.getdata(key, user_session_id)
        returndata = data
    return returndata


# form = cgi.FieldStorage()


# user_response =  form.getvalue('myquery')
# # if not user_response:
# #     user_response = ""
# session_id = "1"


#
# user_response =  form.getvalue('myquery')
# # if not user_response:
# #     user_response = ""
# session_id = "1"

user_response =  sys.argv[1]
session_id = sys.argv[2]

Endwords = ['bye', 'thanks', 'thank you']

if user_response not in Endwords:
    user_input_para = get_data_file('Temp_user_input.json', session_id)

    # ### to check output result from get API
    #print(user_input_para)


    reconsider = user_input_para['reconsider']
    if reconsider is True:
        user_response = user_input_para['user_input'] + " " + user_response
        user_response = user_response.replace('.', "")


def set_okay():
    Okay = ["absolutely", "agreed", "all right", "aye", "by all means", "certainly", "fine", "good", "good enough",
            "just so", "of course", "okay", "positively", "precisely", "sure thing", "surely", "very well", "yea",
            "yep"]
    return random.choice(Okay)

# def ct(str, start): #check_time
#     end = time.time()
#     print("to ",str," :", end - start)

class ChatBotWeb:
    def __init__(self):


        c_obj = ChatBot.ChatBot(session_id)
        c_obj.session_id = session_id

        self.bot_responce = ""

        to_ask_parameters = get_data_file('to_ask.json', c_obj.session_id)
        if to_ask_parameters is '':
            to_ask = False
            to_ask_parameters = {'to_ask': to_ask}
            set_data_file('to_ask.json', c_obj.session_id, to_ask_parameters)
        else:
            to_ask = to_ask_parameters['to_ask']

        flag = True
        self.user_response = user_response
        if self.user_response is not None:

            user_responses = ChatBot.checkand(self.user_response)  # self.user_response.split('and')

            self.t = 0  # Time
            self.index_A = []
            self.index_Ya = []
            parameters = get_data_file('Temp_parameters.json', c_obj.session_id)
            if parameters is '':
                parameters = {'t': self.t, 'index_A': self.index_A, 'index_Ya': self.index_Ya}
                set_data_file('Temp_parameters.json', c_obj.session_id, parameters)
            else:
                self.t = parameters['t']
                self.index_A = parameters['index_A']
                self.index_Ya = parameters['index_Ya']

            for one_responce in user_responses:

                if to_ask is True:
                    if one_responce == 'yes':
                        answers = c_obj.answers
                        answers_array = nltk.sent_tokenize(answers)
                        idy = self.index_Ya[-1]
                        answer = answers_array[idy]
                        bot_response = (answer, flag)
                        self.bot_responce = bot_response[0]
                        self.toprint()
                        to_ask = False
                        to_ask_parameters = {'to_ask': to_ask}
                        set_data_file('to_ask.json', c_obj.session_id, to_ask_parameters)

                    elif one_responce == 'no':
                        answer = set_okay() + '... Do you have any other questions?'
                        bot_response = (answer, flag)
                        self.bot_responce = bot_response[0]
                        self.toprint()
                        to_ask = False
                        to_ask_parameters = {'to_ask': to_ask}
                        set_data_file('to_ask.json', c_obj.session_id, to_ask_parameters)
                    else:
                        # self.t += 1
                        bot_response = c_obj.set_response(one_responce, flag, self.t, c_obj.session_id)
                        self.bot_responce = bot_response[0]
                        self.toprint()

                        if not bot_response[1]:
                            self.t = 0  # Time
                            self.index_A = []
                            self.index_Ya = []
                            parameters = {'t': self.t, 'index_A': self.index_A, 'index_Ya': self.index_Ya}
                            set_data_file('Temp_parameters.json', c_obj.session_id, parameters)

                            self.user_response_array = []
                            data = {'Training_data': self.user_response_array}
                            set_data_file('Training_data.json', c_obj.session_id, data)

                            sql_para_data = {"query": "", "attributes": [], "values": [], "condition": []}
                            set_data_file('sql_para', c_obj.session_id, sql_para_data)

                        to_ask = False
                        to_ask_parameters = {'to_ask': to_ask}
                        set_data_file('to_ask.json', c_obj.session_id, to_ask_parameters)

                else:
                    # self.t += 1

                    bot_response = c_obj.set_response(one_responce, flag, self.t, c_obj.session_id)


                    self.bot_responce = bot_response[0]
                    self.toprint()

                    if not bot_response[1]:
                        self.t = 0  # Time
                        self.index_A = []
                        self.index_Ya = []
                        parameters = {'t': self.t, 'index_A': self.index_A, 'index_Ya': self.index_Ya}
                        set_data_file('Temp_parameters.json', c_obj.session_id, parameters)

                        self.user_response_array = []
                        data = {'Training_data': self.user_response_array}
                        set_data_file('Training_data.json', c_obj.session_id, data)

                        to_ask = False
                        to_ask_parameters = {'to_ask': to_ask}
                        set_data_file('to_ask.json', c_obj.session_id, to_ask_parameters)

                        sql_para_data = {"query": "","attributes": [],"values": [],"condition": []}
                        set_data_file('sql_para', c_obj.session_id, sql_para_data)

                    if self.t % 3 == 0 and self.t != 0:
                        print("<br>")
                        c_obj.get_bot_question()
                        to_ask = True
                        to_ask_parameters = {'to_ask': to_ask}
                        set_data_file('to_ask.json', c_obj.session_id, to_ask_parameters)

    def toprint(self):
        print(self.bot_responce)



if __name__ == '__main__':
    c = ChatBotWeb()

# print("</body></html>")
